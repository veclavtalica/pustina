const std = @import("std");
const sdl = @import("sdl");
const zgl = @import("zgl");
const gfx = @import("gfx.zig");
const gen = @import("gen.zig");
const ppm = @import("ppm.zig");
const vec = @import("vec.zig");
const chunk = @import("chunk.zig");
const raster = @import("raster.zig");
const ChunkHeightmaps = gen.ChunkHeightmaps;
const Vector2 = @import("vec.zig").Vector2;

var was_init: bool = false;
var window: sdl.Window = undefined;
var glctx: sdl.gl.Context = undefined;

pub fn main() !void {
    try init();
    defer deinit();

    try gfx.init();
    // todo: deinit

    var requests: [32 * 32]chunk.Chunk.PositionPlane = undefined;
    for (&requests, 0..) |*request, i| {
        request.* = .{
            .x = @as(u16, @intCast(32 + i % 32)),
            .z = @as(u16, @intCast(32 + i / 32)),
        };
    }

    {
        const request = try ChunkHeightmaps.populate(32, &requests, gen.Region{});
        while (!request.waitForReadiness(1000)) {}
        const result = request.getMappedResult();
        defer {
            request.unmap();
            request.free();
        }

        var gpa = std.heap.GeneralPurposeAllocator(.{}){};
        try ppm.writeChunkHeightmapsResultToFile(
            try std.fs.cwd().createFile("gen.zig.heightmap-generation.ppm", .{}),
            &result,
            &requests,
            gpa.allocator(),
        );
    }

    {
        const request = try ChunkHeightmaps.populate(32, &requests, (gen.Region{}).translateBy(Vector2(i128){ .x = 1 }));
        while (!request.waitForReadiness(1000)) {}
        const result = request.getMappedResult();
        defer {
            request.unmap();
            request.free();
        }

        var gpa = std.heap.GeneralPurposeAllocator(.{}){};
        try ppm.writeChunkHeightmapsResultToFile(
            try std.fs.cwd().createFile("gen.zig.heightmap-generation2.ppm", .{}),
            &result,
            &requests,
            gpa.allocator(),
        );
    }

    {
        const request = try ChunkHeightmaps.populate(32, &requests, (gen.Region{}).translateBy(Vector2(i128){ .y = 1 }));
        while (!request.waitForReadiness(1000)) {}
        const result = request.getMappedResult();
        defer {
            request.unmap();
            request.free();
        }

        var gpa = std.heap.GeneralPurposeAllocator(.{}){};
        try ppm.writeChunkHeightmapsResultToFile(
            try std.fs.cwd().createFile("gen.zig.heightmap-generation3.ppm", .{}),
            &result,
            &requests,
            gpa.allocator(),
        );
    }

    // const heap = try gfx.ChunkBatch.init(gpa.allocator(), gfx.ChunkBatch.Configuration.init(1, 1, 1));
    // _ = heap;

    zgl.viewport(0, 0, 640, 480);

    mainLoop: while (true) {
        while (sdl.pollNativeEvent()) |event| {
            if (event.type == sdl.c.SDL_QUIT)
                break :mainLoop;

            gfx.quad.renderScreenspaceQuads(&[_]gfx.quad.ScreenspaceQuad{.{ .x = 0, .y = 0, .w = 1, .h = 1 }});

            // todo: Make sure swapping doesn't block processing of the next frame.
            //       For that main thread might be delegated fully to rendering.
            sdl.gl.swapWindow(window);

            zgl.invalidateFramebuffer(.draw_buffer, &[_]zgl.FramebufferAttachment{ .default_color, .default_depth, .default_stencil });
            zgl.clear(.{ .depth = true, .stencil = true });
            zgl.flush();
        }
    }
}

pub fn init() !void {
    if (was_init)
        return;

    sdl.c.SDL_SetMainReady();

    try sdl.init(.{ .video = true });

    window = try sdl.createWindow("pustina", .centered, .centered, 640, 480, .{ .context = .opengl });

    // try sdl.gl.setAttribute(.{ .context_profile_mask = .es });
    // try sdl.gl.setAttribute(.{ .context_major_version = 2 });
    // try sdl.gl.setAttribute(.{ .context_minor_version = 0 });
    try sdl.gl.setAttribute(.{ .doublebuffer = true });
    try sdl.gl.setAttribute(.{ .accelerated_visual = true });
    try sdl.gl.setAttribute(.{ .buffer_size = 24 });
    try sdl.gl.setAttribute(.{ .alpha_size = 0 });
    try sdl.gl.setAttribute(.{ .depth_size = 16 });
    try sdl.gl.setAttribute(.{ .stencil_size = 0 });
    try sdl.gl.setAttribute(.{ .accum_red_size = 0 });
    try sdl.gl.setAttribute(.{ .accum_green_size = 0 });
    try sdl.gl.setAttribute(.{ .accum_blue_size = 0 });
    try sdl.gl.setAttribute(.{ .accum_alpha_size = 0 });
    try sdl.gl.setAttribute(.{ .multisamplebuffers = false });

    try sdl.gl.setAttribute(.{
        .context_flags = .{
            .debug = comptime std.debug.runtime_safety,
            // .forward_compatible = true,
            // .robust_access = true,
        },
    });

    glctx = try sdl.gl.createContext(window);

    try zgl.loadExtensions({}, sdlLoadGlProcedure);

    sdl.gl.setSwapInterval(.adaptive_vsync) catch sdl.gl.setSwapInterval(.vsync) catch try sdl.gl.setSwapInterval(.immediate);

    was_init = true;
}

pub fn deinit() void {
    if (!was_init)
        return;

    sdl.gl.deleteContext(glctx);
    _ = window.destroy();
    sdl.quit();
}

fn sdlLoadGlProcedure(ctx: void, name: [:0]const u8) ?*anyopaque {
    _ = ctx;
    return sdl.c.SDL_GL_GetProcAddress(name);
}

test {
    defer deinit();

    _ = @import("chunk.zig");
    _ = @import("gen.zig");
    _ = @import("raster.zig");
}
