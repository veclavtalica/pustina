const std = @import("std");
const zgl = @import("zgl");
const gfx = @import("gfx.zig");
const ppm = @import("ppm.zig");
const res = @import("res.zig");
const shader = @import("shader.zig");
const chunk = @import("chunk.zig");
const Chunk = chunk.Chunk;
const Vector2 = @import("vec.zig").Vector2;

// todo: Should not be hardcoded.
// note: limited to texture rect size right now.
const max_heightmap_chunks_at_a_time = 4096;
pub const heightmap_framebuffer_dimension = 1024;

const heightmap_chunk_value_table_size = 256;
const heightmap_chunk_value_table_count = 4;
const heightmap_chunk_value_table_capacity = heightmap_chunk_value_table_size * heightmap_chunk_value_table_count;

const heightmap_chunk_permutations_seed = 75872937;
/// Enough to have unique value for each cell at grid_scale == 1.
const heightmap_chunk_permutations_dimension = 64;

const heightmap_chunks_pixel_buffer_capacity = @max(
    @sizeOf(Vector2(f32)) * max_heightmap_chunks_at_a_time,
    @sizeOf(u8) * heightmap_chunk_value_table_capacity,
);

var heightmap_chunk_was_init: bool = false;
var heightmap_chunk_noise2d_program: zgl.Program = .invalid;
var heightmap_chunk_array_buffer: zgl.Buffer = .invalid;
var heightmap_chunk_positions: zgl.Texture = .invalid;
var heightmap_chunk_framebuffer: zgl.Framebuffer = .invalid;
var heightmap_chunk_texture: zgl.Texture = .invalid;
var heightmap_chunk_values: zgl.Texture = .invalid;
var heightmap_chunk_permutations: zgl.Texture = .invalid;
// todo: Comptime function to generate getting of uniform locations.
var heightmap_chunk_u_chunk_positions: ?u32 = null;
var heightmap_chunk_u_values: ?u32 = null;
var heightmap_chunk_u_resolution: ?u32 = null;
var heightmap_chunk_u_permutations: ?u32 = null;
var heightmap_chunk_pixel_buffer: zgl.Buffer = .invalid;

// todo: Errdefer to clear state.
// todo: Register deinit handler.
fn initHeightmapChunk() !void {
    try gfx.init();

    const vs = try shader.compileFromResource(.vertex, try res.loadResource("shaders/chunk-heightmap-gen.glsl.vert"));
    defer vs.delete();

    const noise = try shader.compileFromResource(.fragment, try res.loadResource("shaders/noise/multi-domain/perlin-noise2d.glsl.frag"));
    defer noise.delete();

    const fs = try shader.compileFromResource(.fragment, try res.loadResource("shaders/chunk-heightmap-gen.glsl.frag"));
    defer fs.delete();

    heightmap_chunk_noise2d_program = try shader.initProgram(&[_]zgl.Shader{ vs, fs, noise });

    heightmap_chunk_noise2d_program.use();
    {
        heightmap_chunk_u_chunk_positions = zgl.getUniformLocation(heightmap_chunk_noise2d_program, "u_chunk_positions").?;
        heightmap_chunk_u_values = zgl.getUniformLocation(heightmap_chunk_noise2d_program, "u_values").?;
        heightmap_chunk_u_resolution = zgl.getUniformLocation(heightmap_chunk_noise2d_program, "u_resolution").?;
        heightmap_chunk_u_permutations = zgl.getUniformLocation(heightmap_chunk_noise2d_program, "u_permutations").?;

        zgl.uniform1i(heightmap_chunk_u_chunk_positions, 0);
        zgl.uniform1i(heightmap_chunk_u_values, 1);
        zgl.uniform1i(heightmap_chunk_u_permutations, 2);
    }

    heightmap_chunk_array_buffer = zgl.createBuffer();
    heightmap_chunk_array_buffer.bind(.array_buffer);
    {
        zgl.bufferStorage(.array_buffer, u8, 8, &[_]u8{ 0, 0, 1, 0, 0, 1, 1, 1 }, .{});
    }

    heightmap_chunk_positions = zgl.genTexture();
    heightmap_chunk_positions.bind(.rectangle);
    {
        zgl.texParameter(.rectangle, .min_filter, .nearest);
        zgl.texParameter(.rectangle, .mag_filter, .nearest);
        zgl.texParameter(.rectangle, .swizzle_a, .zero);
        // todo: Use Y dimension.
        zgl.texStorage2D(.rectangle, 1, .rg32f, max_heightmap_chunks_at_a_time, 1);
    }

    heightmap_chunk_texture = zgl.genTexture();
    heightmap_chunk_texture.bind(.@"2d");
    {
        zgl.texParameter(.@"2d", .min_filter, .nearest);
        zgl.texParameter(.@"2d", .mag_filter, .nearest);
        // todo: Varying storage dimensions might be used to reduce amount of redundant pixels to transfer.
        //       For this width should shrink as much as possible.
        zgl.texStorage2D(.@"2d", 1, .r16, heightmap_framebuffer_dimension, heightmap_framebuffer_dimension);
    }

    heightmap_chunk_pixel_buffer = zgl.createBuffer();
    heightmap_chunk_pixel_buffer.bind(.pixel_unpack_buffer);
    {
        zgl.bufferStorage(.pixel_unpack_buffer, Vector2(f32), heightmap_chunks_pixel_buffer_capacity, null, .{ .map_write = true });
        zgl.bindBuffer(.invalid, .pixel_unpack_buffer);
    }

    heightmap_chunk_values = zgl.genTexture();
    heightmap_chunk_values.bind(.@"2d");
    {
        zgl.texParameter(.@"2d", .min_filter, .nearest);
        zgl.texParameter(.@"2d", .mag_filter, .nearest);
        zgl.texParameter(.@"2d", .swizzle_a, .zero);
        zgl.texStorage2D(.@"2d", 1, .r8_snorm, heightmap_chunk_value_table_size, heightmap_chunk_value_table_count);
    }

    heightmap_chunk_permutations = zgl.genTexture();
    heightmap_chunk_permutations.bind(.@"2d");
    {
        zgl.texParameter(.@"2d", .min_filter, .nearest);
        zgl.texParameter(.@"2d", .mag_filter, .nearest);
        zgl.texParameter(.@"2d", .swizzle_a, .zero);
        zgl.texStorage2D(.@"2d", 1, .r8, heightmap_chunk_permutations_dimension, heightmap_chunk_permutations_dimension);

        heightmap_chunk_pixel_buffer.bind(.pixel_unpack_buffer);
        {
            defer zgl.bindBuffer(.invalid, .pixel_unpack_buffer);
            {
                var buf = @as([]align(64) u8, @alignCast(zgl.mapBufferRange(
                    .pixel_unpack_buffer,
                    u8,
                    0,
                    heightmap_chunk_permutations_dimension * heightmap_chunk_permutations_dimension,
                    .{ .write = true },
                )));

                defer if (!zgl.unmapBuffer(.pixel_unpack_buffer))
                    @panic("Unmap failed");

                var prng = std.rand.RomuTrio.init(heightmap_chunk_permutations_seed);
                prng.random().bytes(buf);
            }

            zgl.bindTexture(heightmap_chunk_permutations, .@"2d");
            zgl.texSubImage2D(.@"2d", 0, 0, 0, heightmap_chunk_permutations_dimension, heightmap_chunk_permutations_dimension, .red, .unsigned_byte, null);
            zgl.invalidateBufferData(heightmap_chunk_pixel_buffer);
        }
    }

    heightmap_chunk_framebuffer = zgl.genFramebuffer();
    heightmap_chunk_framebuffer.bind(.buffer);
    {
        defer zgl.bindFramebuffer(.invalid, .buffer);
        zgl.viewport(0, 0, heightmap_framebuffer_dimension, heightmap_framebuffer_dimension);
        zgl.framebufferTexture2D(heightmap_chunk_framebuffer, .buffer, .color0, .@"2d", heightmap_chunk_texture, 0);
        zgl.drawBuffers(&[_]zgl.FramebufferAttachment{.color0});
        if (zgl.checkFramebufferStatus(.buffer) != .complete)
            @panic("Incomplete framebuffer");
    }

    heightmap_chunk_was_init = true;
}

fn populateValueTexture(texture: zgl.Texture, texture_target: zgl.TextureTarget, region: Region) void {
    heightmap_chunk_pixel_buffer.bind(.pixel_unpack_buffer);
    {
        defer zgl.bindBuffer(.invalid, .pixel_unpack_buffer);
        {
            var buf = @as([]align(64) u8, @alignCast(zgl.mapBufferRange(
                .pixel_unpack_buffer,
                u8,
                0,
                heightmap_chunk_value_table_capacity,
                .{ .write = true },
            )));

            defer if (!zgl.unmapBuffer(.pixel_unpack_buffer))
                @panic("Unmap failed");

            // todo: Recalculating it each time is really wasteful.
            //       We can cache things both on server and client based on most recent usage.
            var prng = std.rand.RomuTrio.init(region.getSeed());
            prng.random().bytes(buf[0..heightmap_chunk_value_table_size]);

            prng = std.rand.RomuTrio.init(region.translateBy(Vector2(i128){ .y = 1 }).getSeed());
            prng.random().bytes(buf[heightmap_chunk_value_table_size .. heightmap_chunk_value_table_size * 2]);

            prng = std.rand.RomuTrio.init(region.translateBy(Vector2(i128){ .y = 1 }).getSeed());
            // prng = std.rand.RomuTrio.init(region.translateBy(Vector2(i128){ .y = 1 }).getSeed());
            prng.random().bytes(buf[heightmap_chunk_value_table_size * 2 .. heightmap_chunk_value_table_size * 3]);

            prng = std.rand.RomuTrio.init(region.translateBy(Vector2(i128){ .y = 1 }).getSeed());
            // prng = std.rand.RomuTrio.init(region.translateBy(Vector2(i128){ .x = 1, .y = 1 }).getSeed());
            prng.random().bytes(buf[heightmap_chunk_value_table_size * 3 .. heightmap_chunk_value_table_capacity]);
        }

        zgl.bindTexture(texture, texture_target);
        zgl.invalidateTexImage(texture, 0);
        zgl.texSubImage2D(texture_target, 0, 0, 0, heightmap_chunk_value_table_size, heightmap_chunk_value_table_count, .red, .unsigned_byte, null);
        zgl.invalidateBufferData(heightmap_chunk_pixel_buffer);
    }
}

pub const ChunkHeightmaps = struct {
    /// Populated chunk heightmap data laid out linearly.
    /// Note that input parameters are not stored and assumed to be available at call site.
    pub const Result = struct {
        data: []align(64) u16,
        resolution: u8,

        /// `request_index` is an index to original `requests` input.
        pub fn getChunk(self: @This(), request_index: usize) []u16 {
            const resolution_doubled = @as(u16, self.resolution) * self.resolution;
            const column = request_index % (heightmap_framebuffer_dimension / resolution_doubled);
            const row = request_index / (heightmap_framebuffer_dimension / resolution_doubled);
            return self.data[column * resolution_doubled + row * heightmap_framebuffer_dimension .. (column + 1) * resolution_doubled + row * heightmap_framebuffer_dimension];
        }

        // todo: Copy method to duplicate results from server owned DMA into client if needed.
    };

    /// Asynchronous handle to future result of population.
    pub const Response = struct {
        sync: zgl.Sync,
        pixel_buffer: zgl.Buffer,
        /// Amount of texels in response.
        size: usize,
        resolution: u8,

        pub fn free(self: @This()) void {
            zgl.deleteSync(self.sync);
            zgl.deleteBuffer(self.pixel_buffer);
        }

        /// Non-blocking ready check.
        pub fn isReady(self: @This()) bool {
            return self.waitForReadiness(0);
        }

        /// Blocking ready check.
        pub fn waitForReadiness(self: @This(), nanoseconds: usize) bool {
            return switch (zgl.clientWaitSync(self.sync, false, nanoseconds)) {
                .already_signaled, .condition_satisfied => true,
                .timeout_expired => false,
                .wait_failed => @panic("Syncing failed."),
            };
        }

        pub fn getMappedResult(self: @This()) ChunkHeightmaps.Result {
            // todo: Buffer binding smears into caller.
            self.pixel_buffer.bind(.pixel_pack_buffer);
            {
                defer zgl.bindBuffer(.invalid, .pixel_pack_buffer);
                return ChunkHeightmaps.Result{
                    .data = @as([]align(64) u16, @alignCast(zgl.mapBufferRange(.pixel_pack_buffer, u16, 0, self.size, .{ .read = true }))),
                    .resolution = self.resolution,
                };
            }
        }

        /// Should be called after `getMappedResult`. Using result data after this is invalid.
        pub fn unmap(self: @This()) void {
            self.pixel_buffer.bind(.pixel_pack_buffer);
            {
                defer zgl.bindBuffer(.invalid, .pixel_pack_buffer);
                if (!zgl.unmapBuffer(.pixel_pack_buffer))
                    @panic("Unmap failed");
            }
        }
    };

    // todo: Dynamic max_heightmap_chunks_at_a_time, granularity of it will be important for job scheduling.
    pub fn populate(resolution: u8, requests: []const Chunk.PositionPlane, region: Region) !Response {
        // todo: Require it as power of 2.
        std.debug.assert(resolution != 0 and resolution <= 32);
        std.debug.assert(requests.len <= max_heightmap_chunks_at_a_time);

        if (!heightmap_chunk_was_init)
            try initHeightmapChunk();

        populateValueTexture(heightmap_chunk_values, .@"2d", region);

        heightmap_chunk_noise2d_program.use();
        zgl.uniform1f(heightmap_chunk_u_resolution, @as(f32, @floatFromInt(resolution)));

        heightmap_chunk_pixel_buffer.bind(.pixel_unpack_buffer);
        {
            defer zgl.bindBuffer(.invalid, .pixel_unpack_buffer);
            {
                var buf = @as([]align(64) Vector2(f32), @alignCast(zgl.mapBufferRange(
                    .pixel_unpack_buffer,
                    Vector2(f32),
                    0,
                    requests.len,
                    .{ .write = true },
                )));

                defer if (!zgl.unmapBuffer(.pixel_unpack_buffer))
                    @panic("Unmap failed");

                for (buf, 0..) |*item, i| {
                    item.* = Vector2(f32){
                        .x = @as(f32, @floatFromInt(requests[i].x)),
                        .y = @as(f32, @floatFromInt(requests[i].z)),
                    };
                }
            }

            zgl.activeTexture(.texture_0);
            zgl.bindTexture(heightmap_chunk_positions, .rectangle);
            zgl.texSubImage2D(.rectangle, 0, 0, 0, requests.len, 1, .rg, .float, null);
            zgl.invalidateBufferData(heightmap_chunk_pixel_buffer);
        }

        zgl.activeTexture(.texture_1);
        zgl.bindTexture(heightmap_chunk_values, .@"2d");

        zgl.activeTexture(.texture_2);
        zgl.bindTexture(heightmap_chunk_permutations, .@"2d");

        heightmap_chunk_array_buffer.bind(.array_buffer);
        zgl.bindVertexArray(.invalid);
        zgl.vertexAttribPointer(0, 2, .byte, false, 0, 0);
        zgl.enableVertexAttribArray(0);

        heightmap_chunk_framebuffer.bind(.buffer);
        zgl.drawArraysInstanced(.triangle_strip, 0, 4, requests.len);
        zgl.invalidateTexImage(heightmap_chunk_positions, 0);

        const resolution_doubled = @as(u16, resolution) * resolution;
        const layers = 1 + (requests.len - 1) / (heightmap_framebuffer_dimension / resolution_doubled);
        const needed = heightmap_framebuffer_dimension * layers;

        // todo: Try https://github.com/g-truc/ogl-samples/blob/4.5.4/tests/gl-440-fbo-readpixels-staging.cpp
        // todo: Test whether reading from texture that is attached to framebuffer is any different.

        const pixel_buffer = zgl.createBuffer();
        pixel_buffer.bind(.pixel_pack_buffer);
        {
            defer zgl.bindBuffer(.invalid, .pixel_pack_buffer);
            zgl.readBuffer(.color0);
            zgl.bufferStorage(.pixel_pack_buffer, u16, needed, null, .{ .map_read = true, .client_storage = true });
            zgl.readPixels(0, 0, heightmap_framebuffer_dimension, layers, .red, .unsigned_short, null);
        }

        zgl.invalidateFramebuffer(.buffer, &[_]zgl.FramebufferAttachment{.color0});
        zgl.bindFramebuffer(.invalid, .buffer);
        const sync = zgl.fenceSync();
        zgl.flush();

        // todo: Only used for display, instead provide function to bind this.
        zgl.activeTexture(.texture_0);
        heightmap_chunk_texture.bind(.@"2d");

        return Response{
            .sync = sync,
            .pixel_buffer = pixel_buffer,
            .size = needed,
            .resolution = resolution,
        };
    }
};

/// Used for relative seed production in infinite (albeit eventually looping over a *loooong* period) world generation.
pub const Region = struct {
    // https://en.wikipedia.org/wiki/Block_cipher_mode_of_operation#Counter_(CTR)
    counters: Vector2(i128) = Vector2(i128).center,

    /// Chosen so that 1/32 increment is exact in floating point representation unsed by any GLES 2 graphics processing unit.
    const chunks_per_region = 64;
    const root_seed = 61218630118431501;

    // todo: Calculate and cache it on translation / initialization instead?
    pub fn getSeed(self: @This()) u64 {
        const result = std.hash.Wyhash.hash(
            std.hash.Wyhash.hash(root_seed, @as([*]align(@alignOf(i128)) const u8, @ptrCast(&self.counters.x))[0..@sizeOf(i128)]),
            @as([*]align(@alignOf(i128)) const u8, @ptrCast(&self.counters.y))[0..@sizeOf(i128)],
        );
        return result;
    }

    pub fn translateBy(self: @This(), translation: Vector2(i128)) @This() {
        return @This(){
            .counters = self.counters.addWithOverflow(translation),
        };
    }

    // todo: Test equality after back-and-forth.
};

// todo: Test reproducibility between generated chunks in repeated requests and also by gold file.
test "heightmap generation" {
    var requests: [32 * 32]chunk.Chunk.PositionPlane = undefined;
    for (&requests, 0..) |*request, i| {
        request.* = .{
            .x = @as(u16, @intCast(32 + i % 32)),
            .z = @as(u16, @intCast(32 + i / 32)),
        };
    }
    const request = try ChunkHeightmaps.populate(32, &requests, Region{});
    while (!request.waitForReadiness(1000)) {}
    const result = request.getMappedResult();
    defer {
        request.unmap();
        request.free();
    }
    try ppm.writeChunkHeightmapsResultToFile(
        try std.fs.cwd().createFile("gen.zig.heightmap-generation.ppm", .{}),
        &result,
        &requests,
        std.testing.allocator,
    );
}

// todo: Test continuity of regions.
