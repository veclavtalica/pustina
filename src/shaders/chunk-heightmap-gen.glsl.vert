#version 120

#extension GL_ARB_texture_rectangle : require
#extension GL_ARB_draw_instanced : require

attribute vec2 in_position;

// todo: Better be a uniform instead.
#define RENDER_RESOLUTION (1024.0)

uniform sampler2DRect u_chunk_positions;
uniform float u_resolution;

varying vec3 var_position;

void main() {
    var_position.xy = texture2DRect(u_chunk_positions, vec2(gl_InstanceIDARB, 0.0)).xy;
    float offset_y = floor(gl_InstanceIDARB / (RENDER_RESOLUTION / (u_resolution * u_resolution)));
    vec2 offset = vec2(gl_InstanceIDARB + offset_y * (-RENDER_RESOLUTION / (u_resolution * u_resolution)), offset_y);
    var_position.z = offset.x * (u_resolution * u_resolution) + 0.5;
    vec2 scale = vec2((u_resolution * u_resolution) / (RENDER_RESOLUTION / 2.0), 1.0 / (RENDER_RESOLUTION / 2.0));
    gl_Position = vec4(vec2(-1.0) + (in_position + offset) * scale, 0.0, 1.0);
}
