#version 120

uniform float u_resolution;

varying vec3 var_position;

float perlin_noise_2d(in vec2 at, in float grid_scale);

#define OCTAVES (4)

float generate_point(in vec2 x) {    
    const float G = 0.5;
    float f = 1.0;
    float a = 0.5;
    float t = 0.0;
    for (int i = 0; i < OCTAVES; i++) {
        t += a * perlin_noise_2d(x, f);
        f *= 2.0;
        a *= G;
    }
    return t;
}

void main() {
    float screen_position = gl_FragCoord.x - var_position.z;
    float chunk_y = floor(screen_position / u_resolution);
    vec2 chunk_position = vec2(chunk_y * -u_resolution + screen_position, chunk_y);
    vec2 domain_position = var_position.xy + chunk_position * (1.0 / u_resolution);

    // Renormalize to [0,1], as it's the only range we could get with integer data.
    // Fractional precision should be enough anyway.
    gl_FragColor.r = 0.5 + 0.5 * (generate_point(domain_position));
}
