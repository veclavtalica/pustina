#version 120

#extension GL_ARB_texture_rectangle : require
#extension GL_ARB_shader_draw_parameters : require

// todo: GL ES uses different scheme.
#if __VERSION__ >= 130
#define FLAT flat
#else
#define FLAT
#endif

// todo: Test whether passing Y separately is better or not.
// Y is encoded in two bytes.
attribute vec4 in_xyyz;
attribute vec3 in_uv;

uniform sampler2DRect u_matrix;
#define MATRIX_RESOLUTION (1008)
#define MATRICES_PER_ROW (MATRIX_RESOLUTION / 24)

FLAT varying vec3 var_uv;

void main() {
    var_uv = in_uv;

    // todo: Test which is better.
    float draw_whole = floor(float(gl_DrawIDARB / MATRICES_PER_ROW));
    vec2 matrix_coord = vec2((-MATRICES_PER_ROW * draw_whole + float(gl_DrawIDARB)) * 4.0, draw_whole);

    // vec2 matrix_coord = vec2(mod(float(gl_DrawIDARB), MATRICES_PER_ROW) * 4.0 + 0.5,
    //     floor(float(gl_DrawIDARB) / MATRICES_PER_ROW) * 4.0 + 0.5);

    // todo: Ideally it should be possible to share for whole workgroup, massively reducing the work.
    //       At least cache hits are likely to be ideal.
    mat4 matrix = mat4(texture2DRect(u_matrix, matrix_coord),
        texture2DRect(u_matrix, matrix_coord + vec2(1.0, 0.0)),
        texture2DRect(u_matrix, matrix_coord + vec2(2.0, 0.0)),
        texture2DRect(u_matrix, matrix_coord + vec2(3.0, 0.0)));

    gl_Position = matrix * vec4(in_xyyz.x, in_xyyz.y * 256.0 + in_xyyz.z, in_xyyz.w, 1.0);
}
