#version 120

// todo: Our own extension and API polyfilling preprocessor.
//       Or use ARB_shading_language_include.

#extension GL_EXT_texture_array : require

// todo: GL ES uses different scheme.
#if __VERSION__ >= 130
#define FLAT flat
#else
#define FLAT
#endif

// todo: Try texture swizzle extension here.
uniform sampler2DArray u_atlas;

FLAT varying vec3 var_uv;

void main() {
    // todo: Test whether not setting alpha changes things.
    // gl_FragColor = vec4(texture2DArray(u_atlas, var_uv).rgb, 0.0);
    gl_FragColor = texture2DArray(u_atlas, var_uv);
}
