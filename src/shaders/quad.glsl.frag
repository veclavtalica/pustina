#version 120

uniform sampler2D u_texture;

varying vec2 var_uv;

void main() {
	gl_FragColor = texture2D(u_texture, var_uv);
}
