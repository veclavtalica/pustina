const std = @import("std");

pub fn Vector2(comptime T: type) type {
    return struct {
        x: T = 0,
        y: T = 0,

        pub const zero = @This(){ .x = 0, .y = 0 };
        pub const center = @This(){ .x = coordinateCenter(T), .y = coordinateCenter(T) };

        pub fn fromScalar(scalar: T) @This() {
            return @This(){ .x = scalar, .y = scalar };
        }

        pub fn as(self: @This(), comptime To: type) Vector2(T) {
            return Vector2(T){
                .x = @as(To, @intCast(self.x)),
                .y = @as(To, @intCast(self.y)),
            };
        }

        pub fn add(self: @This(), other: @This()) @This() {
            return @This(){ .x = self.x + other.x, .y = self.y + other.y };
        }

        const SignedCounterpart = switch (@typeInfo(T).Int.signedness) {
            .signed => T,
            .unsigned => |int| std.meta.Int(.signed, int.bits),
        };

        pub fn addSigned(self: @This(), other: Vector2(SignedCounterpart)) @This() {
            return @This(){
                .x = @as(T, @intCast(@as(RelativeDifference, @intCast(self.x)) + other.x)),
                .y = @as(T, @intCast(@as(RelativeDifference, @intCast(self.y)) + other.y)),
            };
        }

        pub fn addWithOverflow(self: @This(), other: @This()) @This() {
            return @This(){
                .x = @addWithOverflow(self.x, other.x).@"0",
                .y = @addWithOverflow(self.y, other.y).@"0",
            };
        }

        pub fn subtract(self: @This(), other: @This()) @This() {
            return @This(){ .x = self.x - other.x, .y = self.y - other.y };
        }

        pub fn absoluteDifference(self: @This(), other: @This()) @This() {
            return switch (@typeInfo(T).Int.signedness) {
                .signed => self.subtract(other),
                .unsigned => {
                    var result: @This() = undefined;
                    if (self.x >= other.x) {
                        result.x = self.x - other.x;
                    } else {
                        result.x = other.x - self.x;
                    }
                    if (self.y >= other.y) {
                        result.y = self.y - other.y;
                    } else {
                        result.y = other.y - self.y;
                    }
                    return result;
                },
            };
        }

        const RelativeDifference = switch (@typeInfo(T).Int.signedness) {
            .signed => T,
            .unsigned => |int| std.meta.Int(.signed, int.bits + 1),
        };

        // todo: Unit test.
        /// Note that for unsigned types it returns signed components with 1 bit extended as required fod holding all possible cases.
        pub fn relativeDifference(self: @This(), other: @This()) Vector2(RelativeDifference) {
            return switch (@typeInfo(T).Int.signedness) {
                .signed => self.subtract(other),
                .unsigned => {
                    var result: Vector2(RelativeDifference) = undefined;
                    if (self.x >= other.x) {
                        result.x = self.x - other.x;
                    } else {
                        result.x = -@as(RelativeDifference, @intCast(other.x - self.x));
                    }
                    if (self.y >= other.y) {
                        result.y = self.y - other.y;
                    } else {
                        result.y = -@as(RelativeDifference, @intCast(other.y - self.y));
                    }
                    return result;
                },
            };
        }

        pub fn distanceSquared(self: @This()) usize {
            return switch (@typeInfo(T).Int.signedness) {
                .signed => {
                    const x = @as(usize, std.math.absCast(self.x));
                    const y = @as(usize, std.math.absCast(self.y));
                    return x * x + y * y;
                },
                .unsigned => {
                    const xs = @as(usize, self.x) * self.x;
                    const ys = @as(usize, self.y) * self.y;
                    return xs + ys;
                },
            };
        }
    };
}

pub fn Vector3(comptime T: type) type {
    return struct {
        x: T = 0,
        y: T = 0,
        z: T = 0,

        pub const zero = @This(){ .x = 0, .y = 0, .z = 0 };
        pub const center = @This(){ .x = coordinateCenter(T), .y = coordinateCenter(T), .z = coordinateCenter(T) };

        pub fn fromScalar(scalar: T) @This() {
            return @This(){ .x = scalar, .y = scalar, .z = scalar };
        }

        pub fn add(self: @This(), other: @This()) @This() {
            return @This(){ .x = self.x + other.x, .y = self.y + other.y, .z = self.z + other.z };
        }

        pub fn subtract(self: @This(), other: @This()) @This() {
            return @This(){ .x = self.x - other.x, .y = self.y - other.y, .z = self.z - other.z };
        }
    };
}

fn coordinateCenter(comptime T: type) T {
    return switch (@typeInfo(T).Int.signedness) {
        .signed => 0,
        .unsigned => std.math.maxInt(T) / 2,
    };
}
