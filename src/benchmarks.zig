const std = @import("std");
const benchmark = @import("bench").benchmark;
const gen = @import("gen.zig");
const Chunk = @import("chunk.zig").Chunk;

pub fn main() !void {
    try benchmark(struct {
        pub const min_iterations = 100;
        pub const max_iterations = 100;

        pub fn lockstepChunkHeightmap() !void {
            var requests: [32 * 32]Chunk.PositionPlane = undefined;
            for (&requests, 0..) |*request, i| {
                request.* = .{
                    .x = @as(u16, @intCast(i % 32)),
                    .z = @as(u16, @intCast(@divTrunc(i, 32))),
                };
            }
            const request = try gen.ChunkHeightmaps.populate(32, &requests, gen.Region{});
            while (!request.waitForReadiness(1000)) {}
            const result = request.getMappedResult();
            _ = result;
            defer {
                request.unmap();
                request.free();
            }
        }
    });

    try benchmark(struct {
        pub const min_iterations = 50;
        pub const max_iterations = 50;

        pub fn twoAsyncChunkHeightmaps() !void {
            var requests: [32 * 32]Chunk.PositionPlane = undefined;
            for (&requests, 0..) |*request, i| {
                request.* = .{
                    .x = @as(u16, @intCast(i % 32)),
                    .z = @as(u16, @intCast(@divTrunc(i, 32))),
                };
            }
            const requestLeft = try gen.ChunkHeightmaps.populate(32, &requests, gen.Region{});
            const requestRight = try gen.ChunkHeightmaps.populate(32, &requests, gen.Region{});
            while (!requestLeft.waitForReadiness(1000) or !requestRight.waitForReadiness(1000)) {}
            const resultLeft = requestLeft.getMappedResult();
            const resultRight = requestRight.getMappedResult();
            _ = resultLeft;
            _ = resultRight;
            defer {
                requestLeft.unmap();
                requestLeft.free();
                requestRight.unmap();
                requestRight.free();
            }
        }
    });
}
