const std = @import("std");
const zgl = @import("zgl");
const gpu = @import("gpu.zig");
const main = @import("main.zig");
const shader = @import("shader.zig");
const Vector2 = @import("vec.zig").Vector2;
const Vector3 = @import("vec.zig").Vector3;

pub const quad = @import("gfx/quad.zig");
pub const chunk = @import("gfx/chunk.zig");

// todo: Investigate non-array attribute values as an alternative to uniforms.

pub const vertices_per_quad = 4;
pub const elements_per_quad_face = 6;
pub const elements_per_cuboid = elements_per_quad_face * elements_per_quad_face;
pub const vertices_per_cuboid = 12;
pub const matrix_texture_resolution = 1008;
pub const matrix_texture_resolution_layers = 64; // todo: Do we need this?

pub var reusable_vertex_buffer: zgl.Buffer = .invalid;

pub var max_texture_size: u32 = undefined;
pub var max_rectangle_texture_size: u32 = undefined;

var was_init: bool = false;

pub fn init() !void {
    if (was_init)
        return;

    try main.init();

    // todo: Fallback to legacy extension checking.
    // if (!zgl.hasExtension("ARB_map_buffer_alignment"))
    //     @panic("No ARB_map_buffer_alignment support.");

    max_texture_size = @as(u32, @intCast(zgl.getInteger(.max_texture_size)));
    max_rectangle_texture_size = @as(u32, @intCast(zgl.getInteger(.max_rectangle_texture_size)));

    if (comptime std.debug.runtime_safety)
        zgl.debugMessageCallback({}, debugHandle);

    // zgl.enable(.cull_face);
    // zgl.enable(.depth_test);
    // zgl.depthFunc(.less_or_equal);
    zgl.stencilMask(0);

    reusable_vertex_buffer = zgl.createBuffer();

    try quad.init();
    errdefer quad.deinit();

    try chunk.init();
    errdefer chunk.deinit();

    was_init = true;
}

// todo: Color based on severity.
fn debugHandle(
    source: zgl.DebugSource,
    msg_type: zgl.DebugMessageType,
    id: usize,
    severity: zgl.DebugSeverity,
    message: []const u8,
) void {
    _ = source;
    _ = msg_type;
    _ = id;
    _ = severity;
    const stdout = std.io.getStdOut().writer();
    stdout.print("-OpenGL message: {s}\n", .{message}) catch {};
}
