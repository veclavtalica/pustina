/// Block identificator which can represent two types of objects:
/// If no DynamicBit is set, then each such block is identical between each other
/// and described by either global descriptor table, or, possible, chunk local one.
/// In case of DynamicBit presence pointed object has the ability to store unique values.
/// To facilitate this chunk local dynamic storage is allocated, and Id is used for lookup.
/// Use cases for such dynamic objects could be in-world containers and growing plants.
pub const Id = u16;

pub const DynamicBit = 1 << (@bitSizeOf(Id) - 1);
