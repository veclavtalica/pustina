#!/usr/bin/sh

set -e

zig build benchmark
$(find ./ -name benchmarks -type f -printf "%T@ %p\n" | sort -n | cut -d' ' -f 2- | tail -n 1)
