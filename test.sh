#!/usr/bin/sh

set -e

zig build test
$(find ./ -name test -type f -printf "%T@ %p\n" | sort -n | cut -d' ' -f 2- | tail -n 1)
